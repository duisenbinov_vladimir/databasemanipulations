package com.example.databasemanipulations;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public boolean region_text_empty = true;
    public boolean region_number_empty = true;
    public String KZREGION;
    public Long REGION_NUMBER;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button selectButton = (Button) findViewById(R.id.selectButton);
        final Button insertButton = (Button) findViewById(R.id.insertButton);

        final EditText insertRegionText = (EditText) findViewById(R.id.insertRegionText);
        final EditText insertRegionNumber = (EditText) findViewById(R.id.insertRegionNumber);

        insertRegionText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    region_text_empty = false;

                    KZREGION = s.toString();
                } else {
                    region_text_empty = true;

                    KZREGION = "";
                }

                if (!region_number_empty && !region_text_empty) {
                    insertButton.setEnabled(true);
                } else {
                    insertButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        insertRegionNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    region_number_empty = false;

                    REGION_NUMBER = Long.parseLong(s.toString());
                } else {
                    region_number_empty = true;

                    REGION_NUMBER = (long) 0;
                }

                if (!region_number_empty && !region_text_empty) {
                    insertButton.setEnabled(true);
                } else {
                    insertButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues values = new ContentValues();
                values.put("KZREGION", KZREGION);
                values.put("REGION_NUMBER" , REGION_NUMBER);

                Uri uri = getContentResolver().insert(Uri.parse("content://com.example.databasemanipulations.ExampleProvider/CITIESKZ"), values);

                insertRegionText.setText("");
                insertRegionNumber.setText("0", TextView.BufferType.EDITABLE);
                insertRegionNumber.getText().clear();
            }
        });

        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                @SuppressLint("Recycle") Cursor cursor = getContentResolver().query(Uri.parse("content://com.example.databasemanipulations.ExampleProvider/CITIESKZ/1"), null, null, null, null);

                    ArrayList<String> itemsArray = new ArrayList<>();
                    ListView listView = findViewById(R.id.selectItems);

                    if (cursor != null) {
                        while (cursor.moveToNext()) {
                            itemsArray.add(cursor.getString(0) + ": " + cursor.getString(1) + " (" + cursor.getString(2) + ")");
                        }
                        ListAdapter listAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, itemsArray);
                        listView.setAdapter(listAdapter);
                    }
            }
        });
    }
}
