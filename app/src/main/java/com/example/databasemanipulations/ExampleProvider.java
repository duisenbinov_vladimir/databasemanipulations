package com.example.databasemanipulations;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.util.HashMap;
import java.util.Objects;

public class ExampleProvider extends ContentProvider {

    static final String PROVIDER_NAME = "com.example.databasemanipulations.ExampleProvider";
    static final String URL = "content://" + PROVIDER_NAME + "/CITIESKZ";
    static final Uri CONTENT_URI = Uri.parse(URL);

    static final String _ID = "_id";
    static final String KZREGION = "KZREGION";
    static final String REGION_NUMBER = "REGION_NUMBER";
    private static HashMap<String, String> CITIESKZ_PROJECTION_MAP;

    public DatabaseActivity databaseHelper;
    public static final String DATABASE_NAME = "First Database";
    private SQLiteDatabase db;

    static final int CITIES = 1;

    static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "CITIESKZ", CITIES);
    }

    public class DatabaseActivity extends SQLiteOpenHelper {

        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "First Database";
        public static final String TABLE_NAME = "CITIESKZ";

        public DatabaseActivity(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE CITIESKZ ("
                    + " _id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + " KZREGION TEXT,"
                    + " REGION_NUMBER TEXT);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP IF TABLE EXISTS " + DATABASE_NAME);
        }
    }

    @Override
    public boolean onCreate() {
        this.databaseHelper = new DatabaseActivity(getContext());
        db = databaseHelper.getWritableDatabase();

        return db != null;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri,
                        @Nullable String[] projection,
                        @Nullable String selection,
                        @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables("CITIESKZ");

        if (uriMatcher.match(uri) == CITIES) {
            qb.setProjectionMap(CITIESKZ_PROJECTION_MAP);
        }

        if (sortOrder == null || sortOrder == "") {
            sortOrder = KZREGION;
        }
        Cursor cursor = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(Objects.requireNonNull(getContext()).getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        long rowID = db.insert("CITIESKZ", "", values);

        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }

        throw new SQLException("Error while inserting to uri: " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
